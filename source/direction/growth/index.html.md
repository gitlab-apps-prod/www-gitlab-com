---
layout: markdown_page
title: Product Direction - Growth
description:
---

## Growth Section Overview

The growth team at GitLab was formed as of August 2019 and we are iterating along the way. Currently, we have 6 groups (acquistion, conversion, retention, expansion, fullfillment, telemetry), each consisting of a cross-functional team of PM, Dev, UX/Designer, with shared analytics, QA and user research functions.


In essence, we are a product team with a unique focus and approach. 

**While most traditional product team focus on creating value for users via shipping useful products and features, growth team focuses on connecting users with that value.  We do so by**
* Drive feature adoption by removing barrier/providing guidance
* Lower Customer acquistion cost (CAC) by maximizing conversion rate along user journey
* Increase LTV by increasing retention & expansion
* Lower sales/support cost by using product to do the work

**Growth Team also interacts and collabrates closely with many other functional teams, such as Sales, Marketing, Customer success etc. Much of growth team's work help compliment and maximize these team's work, and ultimately help customers to get value from our product. To provide some examples**
* While Marketing Team works on creating awareness and generating demand via channels such as webinars, contents, Growth team can help lower friction in signup and trial flow to maxmize the conversion rate, thus generating higher ROI on marketing spend
* While Sales Team work on convert high value customer in a high touch manner, Growth Team can help create self-serve flows and funnels to convert SMB customers, as well as building automated tools to faciliate sales process 
* While Core product Team works on building features to solve customer problem, Growth Team can help drive product engagement, creat habit and promote multi-stage adoption
* Whiile Customer Success & Support Team works on solving customers problems and helping customers get value from our products, Growth Team can help build new user onboarding flow and on-context support

**Growth Team uses a highly data & experimental driven approach**
*  We start from clearly defining what is success and how to measure it, by picking a North Star Metric - the one metric that matters most now
*  Then we break down the NSM into sub-metrics, this process is called building a growth model 
*  Then by reviewing the sub-metrics, we identify where has the highest potential and where we want to focus on
*  Then we use the build-measure-learn framework to test different ideas that can improve the focus area 


## Growth OKRs & KPIs

**FY21 Q1 OKR**

**Growth Team Only**

Director of Product, Growth: Improve the customer experience with billing related workflows. gitlab-com&263
* Director of Product, Growth KR: Make material improvements to direct signup, trial, seat true-up, upgrade, and renewal workflows, both for .com and self-hosted customers. gitlab-com/Product#725
* Director of Product, Growth KR: Drive Support Satisfaction scores, filtered for billing, to >95%. gitlab-com/Product#726
* Director of Product, Growth KR: Drive $561k in incremental IACV. gitlab-com/Product#727

Director of Product, Growth: Ensure accurate data collection and reporting of AMAU and SMAU metrics. gitlab-com&265
* Director of Product, Growth KR: Deliver AMAU and SMAU tracking with less than 2% uncertainty. gitlab-com/Product#729 

**Across Product Team**

VP of Product Management: Proactively validate problems and solutions with customers. gitlab-com&266
* VP of Product Management KR: At least 2 validation cycles completed per Product Manager. gitlab-com/Product#730

VP of Product Management: Create walk-throughs of the top competitor in each stage to compare against our own demos completed in Q4 or Q1. gitlab-com&267
* VP of Product Management KR: Deliver one recorded walk-through for each stage. gitlab-com/Product#731 

## Growth Team Focus Area

### 1. Key User workflows: 
*  Exploration on about.gitlab.com to understand GitLab product offering
*  Signup process of a free account, a trial, or a paid plan
*  Current customer renewal flow 
*  Current customer paid plan upgrade/downgrade, add-on purchase.
*  In addition, Telemetry team is focused on building frameworks to enable product analytics

### 2. Key User Focus:

#### External
The external user personas on GitLab.com and our Self-Managed instances are very different and should be treated as such. We will be focusing on the 2 described below. 

In addition to the personas, it's also important to understand the permissions available for users (limits and abilities) at each level. 
* [Handbook page on permissions in GitLab](https://about.gitlab.com/handbook/product/#permissions-in-gitlab)
* [Docs on permission on docs.gitlab.com](https://docs.gitlab.com/ee/user/permissions.html)
* *Note: GitLab.com and Self-Managed permission do differ.*  

#### GitLab Team Members
*  Sales Representatives
*  Customer Success Representatives
*  Support Representatives

### 3. Apps & Services we focus on:
*   [GitLab Enterprise Edition (Self-Managed)](https://about.gitlab.com/handbook/engineering/projects/#gitlab)
*   [GitLab-com (SaaS)](https://gitlab.com/gitlab-com/www-gitlab-com#www-gitlab-com)
*   [Version.GitLab](https://about.gitlab.com/handbook/engineering/projects/#version-gitlab-com)
*   [Customers.GitLab](https://about.gitlab.com/handbook/engineering/projects/#customers-app)
*   [License.GitLab](https://about.gitlab.com/handbook/engineering/projects/#license-app)
*   [About.GitLab.com](https://about.gitlab.com/)

## What's Next

12.10


* [Acquisition] Pricing page test
* [Retention] Update the Renewal Banner Appearance, Targeting, and Dismissal Behavior
* [Retention] What's New MVC
* [Retention] Create Additional Renewal Emails
* [Retention] Update .com Billing Page for Auto-renew Case
* [Retention] Better Error Messaging When Applying a License
* [Retention] Add a link to the portal on /admin/license page
* [Expansion] Improve Purchase CI minutes screen
* [Expansion] Add a notification dot when 'buy ci minutes' button is displayed in the user settings on GitLab.com
* [Expansion] Add a 'buy ci minutes' option in the top right drop down (user settings menu) on GitLab.com
* [Telemetry] Query Optimizations: gitlab-org/telemetry#308
* [Telemetry] Harden Usage Ping gitlab-org/telemetry#335
* [Telemetry] Documentation: gitlab-org/telemetry#307


13.0 (Not finalized)
* [Retention] What's New
* [Retention] Apply Cancel Flow to For Self-Managed Subscriptions
* [Telemetry] Any other enhancements deemed necessary for Usage Ping in support of SMAU/AMAU after 12.10
* [Telemetry] Hybrid Logs and Postgres Events w/ Usage Ping: gitlab-org/telemetry#333
* [Telemetry] Add page_type to custom context in our snowplow pageview tracker: gitlab-org/telemetry#110


## Growth Issue Board

* Acquisition board
* Conversion board
* Retention Board
* Fulfillment Board 
* Telemetry Board 
* Expansion Board 

### Helpful Links
*   [Growth Section](https://about.gitlab.com/handbook/engineering/development/growth/)
*   [Growth Product Handbook](https://about.gitlab.com/handbook/product/growth/)
*   [UX Scorecards for Growth](https://gitlab.com/groups/gitlab-org/-/epics/2015)
*   [GitLab Growth project](https://gitlab.com/gitlab-org/growth)
*   [KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)
