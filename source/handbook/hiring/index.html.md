---
layout: handbook-page-toc
title: "Hiring"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hiring pages

- [Greenhouse](/handbook/hiring/greenhouse/)
- [Principles](/handbook/hiring/principles/)
- [Job families](/handbook/hiring/job-families/)
- [Vacancies](/handbook/hiring/vacancies/)
- [Recruiting Alignment](/handbook/hiring/recruiting-alignment/)
- [Interviewing](/handbook/hiring/interviewing/)
- [Sourcing](/handbook/hiring/sourcing/)
- [Job offers and post-interview processes](/handbook/hiring/offers/)
- [Recruiting Process Framework](/handbook/hiring/recruiting-framework/)
- [Referral Process](/handbook/hiring/referral-process/)
- [Referral Operations](/handbook/hiring/referral-operations/)
- [Recruiting Metrics Process](/handbook/hiring/metrics/)
- [Hiring Charts](/handbook/hiring/charts/)
- [Preferred Companies to Recruit from](/handbook/hiring/preferred-companies/)
- [Data Driven Recruiting](/handbook/hiring/data-driven-recruiting)
- [Diversity & Inclusion Recruiting Initiatives](/handbook/hiring/d-&-i-recruiting-initiatives)

Potential applicants should refer to the [jobs FAQ page](/jobs/faq/).

## Related to hiring

- [GitLab talent ambassador](/handbook/hiring/gitlab-ambassadors/)
- [Current vacancies](/jobs/apply/)
- [Contracts](/handbook/contracts)
- [Benefits](/handbook/benefits/)
- [Compensation](/handbook/total-rewards/compensation/)
- [Stock options](/handbook/stock-options)
- [Visas](/handbook/people-group/visas/)
- [Background checks](/handbook/people-group/code-of-conduct/#background-checks)
- [Onboarding](/handbook/general-onboarding)

## Definitions

Job families and vacancies are different things and can't be used interchangeably. Hopefully this information will help to clarify the differences.

- A [job family](/job-families) is a permanent item; its content is a superset of all vacancies for the job family, and it is created with a merge request. Since it is permanent please don't include text that becomes outdated when we hire someone, for example: "We are seeking".
- A [vacancy](/handbook/hiring/vacancies/) is a temporary item posted on Greenhouse; its content is a subset of the job family, and it is created by copying parts of a job family based on an issue.

We don't use the word "job" to refer to a job family or vacancy because it is ambiguous.

People at GitLab can be a specialist on one thing and expert in many:

- A [specialization](/company/team/structure/#specialist) is specific to a job family, each team member can have only one, it defined on the relevant job family page. A specialist uses the compensation benchmark of the job family.
- An [expertise](/company/team/structure/#expert) is not specific to a job family, each team member can have multiple ones, the expertises a team member has are listed on our team page.

The example below shows how we describe what someone does at GitLab:

```
1. Level: Senior
1. Job family: Developer
1. Specialist: Gitaly specialist
1. Location: EMEA
1. Expert: Reliability, Durability
```

We use the following terms to refer to a combination of the above:

### Title

Level and job family as listed on the contract, for example: Senior Developer

### Headline

All parts except expertise, as listed on vacancies, for example: Senior Developer, Gitaly specialist, EMEA

Please use the same order as in the examples above, a few notes:

- Level comes before job family.
- Specialist comes after job family and always includes 'specialist'.
- Location comes after specialization.

We preface a title with "interim" when we're hiring for the position.

## Equal Employment Opportunity

 Diversity & Inclusion is one of GitLab's core [values](/handbook/values) and
 GitLab is dedicated to providing equal employment opportunities (EEO) to all team members
 and candidates for employment without regard to race, color, religion, gender,
 national origin, age, disability, or genetics. One example of how put this into practice
 is through sponsorship of [diversity events](/blog/2016/03/24/sponsorship-update/)

 GitLab complies with all applicable laws governing nondiscrimination in employment. This policy applies to all terms and conditions of employment, including recruiting, hiring, placement, promotion, termination, layoff, recall, transfer,
 leaves of absence, compensation, and training. GitLab expressly prohibits any form of workplace harassment.
 Improper interference with the ability of GitLab’s team members to perform their role duties
 may result in discipline up to and including discharge. If you have any complaints, concerns,
 or suggestions to do better please [contact People Business Partner](/handbook/people-group/#reach-peopleops).

## Country Hiring Guidelines

Please go to [country hiring guidelines](/jobs/faq/#country-hiring-guidelines) for more information.

## Hiring best practices for hiring managers

### Source candidates
* Encourage your team and the team's stakeholders to source candidates. Explore our [Sourcing page](https://about.gitlab.com/handbook/hiring/sourcing/) and reach out to your Recruiting partner to get started.
* Do a [source-a-thon](/handbook/hiring/sourcing/#source-a-thons) with your team to help the sourcing team find more candidates.
* Become a [GitLab Talent Ambassador](https://about.gitlab.com/handbook/hiring/gitlab-ambassadors/).

### Optimize the recruiting process
* Coordinate with other interviewers to ensure that the interview process covers a range of scenarios and minimizes duplication of questions.
* If there is a reasonably high likelihood of a candidate passing the next scheduled interview, ask the coordinator to schedule the next interview after the currently scheduled one. That parallelizes the process and reduces the total time to vet candidates.

### Optimize the reference check process

* Do reference checks via audio or video rather than email. This tends to provide more useful feedback on candidates. Using a 15 minute Calendly meeting to coordinate this is recommended.
* Encourage the candidate to tell their references that they will be hearing from you. This increases the likelihood that they will respond.
* In the email to the reference, include the candidate's full name and purpose of the email (to schedule a reference call).  This also increases the likelihood that they will respond.
* Reference checks should be done by the hiring manager (not the manager's manager) so that the manager is fully invested in the decision.

### Optimize the offer approval process

The items below should be verified before the justification is completed as they are among the common reasons for an offer approval to be rejected or delayed.

* If the candidate has less management experience for a management role than what we generally expect, be sure to explain why they are still a great fit in the justification.
* If the hiring manager did not do the reference checks, explain why in the justification (as the hiring manager should be invested in the decision to hire).
* Confirm the candidate meets the longevity requirement.
* Confirm that all questions in all Greenhouse interview forms are not left empty. If some are left blank, it may give the reviewer the impression that the hiring process was not followed.
* If any interviews are listed but were not performed, ask the recruiter to delete the interview record.  
* If the candidate has a side-job (consulting or similar), explain in the justification section how it was determined that this side-job will not interfere with the candidate's responsibilities at GitLab.

#### Engineering specific

[Engineering hiring practices](/handbook/engineering/#hiring-practices)

* Validate that the "nice to have" attributes have been evaluated and that there are >=5 with a yes vote represented in the justification so that this requirement can be confirmed.
* Confirm there are two "strong yes" votes from someone in engineering.
